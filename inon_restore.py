#!/usr/bin/env python3.6

import pickle
import sqlite3
import pathlib
import argparse


# Monkey patching
pathlib.PurePath.append_suffix = lambda self, suffix: self.parent / f'{self.name}{suffix}'


def load_backup_state(backup_file_prefix):
    state_path = backup_file_prefix.append_suffix('.backupstate')
    try:
        with open(state_path, 'rb') as state_file:
            return pickle.load(state_file)
    except:
        return {}


class UnpicklerState:
    def __init__(self, unpickler):
        self.unpickler = unpickler
        self.data_iterator = None
        self.finished = False

    def load_more(self):
        self.data_iterator = iter(self.unpickler.load())
        return self.data_iterator


def iter_data(unpickler_state, count):
    data_iterator = unpickler_state.data_iterator
    if data_iterator is None:
        data_iterator = unpickler_state.load_more()

    while count > 0:
        try:
            yield next(data_iterator)
            count -= 1
        except StopIteration:
            try:
                data_iterator = unpickler_state.load_more()
            except EOFError:
                unpickler_state.finished = True
                return


def unpickle_database(db_path, table, backup_state_path):
    backup_file_prefix = backup_state_path.with_suffix('')
    pickle_path = backup_file_prefix.append_suffix(f'-{table}.pickle')

    state = load_backup_state(backup_file_prefix).get(table, {})
    if not state:
        print('Unable to read backup state')
        return

    print(f'About to restore {state["last_row_id"]} records')

    with sqlite3.connect(str(db_path)) as connection, open(pickle_path, 'rb') as pickle_file:
        schema = state.get('schema', [])
        if not schema:
            print('Warning, schema is empty')
        for sql in schema:
            connection.execute(sql[0])

        print('Schema restored')

        connection.execute('PRAGMA synchronous=OFF;')

        unpickler_state = UnpicklerState(pickle.Unpickler(pickle_file))

        #TODO: get the column number right
        insert_statement = f'INSERT INTO {table} VALUES ({",".join(["?" for i in range(5)])});'
        chunk_size = 51000
        total = 0

        while not unpickler_state.finished:
            connection.executemany(insert_statement, iter_data(unpickler_state, chunk_size))
            connection.commit()
            total += chunk_size
            print(f'{chunk_size} records committed')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Incremental SQLite database backup for append-only tables.')
    parser.add_argument('backup_state', type=pathlib.Path,
            help='Backup state file')
    parser.add_argument('database', type=pathlib.Path,
            help='New SQLite database filename')
    parser.add_argument('tables', nargs='+', metavar='table',
            help='Table to be restored')

    args = parser.parse_args()
    print(args)

    print(f'Restoring tables to {args.database}')
    for table in args.tables:
        print(f'Restoring {table}: ')
        unpickle_database(args.database, table, args.backup_state)

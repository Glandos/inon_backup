Insert-only SQL backup
======================

Export newly added entries in another file.

Why?
---
Some SQL database tables are designed to be "insert-only". They only grows, and there are never called with "DELETE" or "UPDATE" statement.
When they begin to be very large (the actual figure depends on your hardware), backups becomes to be tricky:

  - Dumping SQL tables from scratch is heavy
  - Backuping the binary file from your SQL engine can be dangerous, without locking
  - If it's not dangerous, binary files are usually not good candidate for deduplicating backup tools

Thus, `inon_backup` will dump tables using Python's pickle format, and recording the last row encountered.
On every calls, it will only get records higher than the last row, and serialize them with an append to the previous file.

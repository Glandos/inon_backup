#!/usr/bin/env python3.6

import sqlite3
import pickle
import pathlib
import tempfile
import fcntl
import os
import argparse

# Monkey patching
pathlib.PurePath.append_suffix = lambda self, suffix: self.parent / f'{self.name}{suffix}'

def get_table_schema(connection, table):
    cursor = connection.execute('SELECT sql FROM sqlite_master WHERE sql IS NOT NULL and tbl_name = :table;', {'table': table})
    return cursor.fetchall()


def get_backup_file_prefix(db_path, backup_directory):
    if backup_directory is None:
        return db_path
    else:
        return pathlib.Path(backup_directory) / db_path.name


def load_backup_state(backup_file_prefix):
    state_path = backup_file_prefix.append_suffix('.backupstate')
    try:
        with open(state_path, 'rb') as state_file:
            return pickle.load(state_file)
    except:
        return {}


def save_backup_state(backup_file_prefix, state):
    state_path = backup_file_prefix.append_suffix('.backupstate')
    with open(state_path, 'wb') as state_file:
        pickle.dump(state, state_file)


def pickle_database(db_path, table, backup_directory=None):
    backup_file_prefix = get_backup_file_prefix(db_path, backup_directory)
    pickle_path = backup_file_prefix.append_suffix(f'-{table}.pickle')

    state = load_backup_state(backup_file_prefix)

    last_row_id = state.get(table, {}).get('last_row_id', -1)

    with sqlite3.connect(str(db_path)) as connection, open(pickle_path, 'a+b') as pickle_file:
        # Check schema
        table_schema = get_table_schema(connection, table)
        saved_schema = state.get(table, {}).get('schema', '')
        truncate_file = False

        if table_schema != saved_schema:
            print(f'Database schema has changed to {table_schema}, starting full dump')
            truncate_file = True
            last_row_id = -1


        with tempfile.NamedTemporaryFile(dir=pickle_path.parent, prefix=pickle_path.name) as temp_file:
            pickler = pickle.Pickler(temp_file, protocol=4)

            cursor = connection.execute(f'select * from {table} where id > :last_row_id;', {'last_row_id': last_row_id})
            cursor.arraysize = 1000

            rows = cursor.fetchmany()
            while rows:
                # VERY IMPORTANT to clear the memo table
                pickler.memo.clear()
                pickler.dump(rows)
                last_row_id = rows[-1][0]
                rows = cursor.fetchmany()

            # Append tmp data to the main backup
            fcntl.lockf(pickle_file, fcntl.LOCK_EX)
            temp_file.seek(0)
            if truncate_file:
                pickle_file.truncate(0)
            pickle_file.seek(0, os.SEEK_END)
            data = temp_file.read(8192)
            while data:
                pickle_file.write(data)
                data = temp_file.read(8192)

            fcntl.lockf(pickle_file, fcntl.LOCK_UN)

    if last_row_id > -1:
        state[table] = {'last_row_id': last_row_id, 'schema': table_schema}
        save_backup_state(backup_file_prefix, state)

    return last_row_id


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Incremental SQLite database backup for append-only tables.')
    parser.add_argument('--backup-directory', type=pathlib.Path,
            help='Backup directory')
    parser.add_argument('database', type=pathlib.Path,
            help='SQLite database filename')
    parser.add_argument('tables', nargs='+', metavar='table',
            help='Table to be dumped')

    args = parser.parse_args()

    print(f'Dumping tables from {args.database}')
    for table in args.tables:
        print(f'Dumping {table} table: ', end='')
        last_row_id = pickle_database(args.database, table, args.backup_directory)
        print(f'last row id is now {last_row_id}')
